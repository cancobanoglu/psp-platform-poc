package com.finartz.psp.transaction.api;

import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

@Value
@JsonDeserialize
public final class TransactionCreatedRequest {

  public final String message;

  @JsonCreator
  public TransactionCreatedRequest(String message) {
    this.message = Preconditions.checkNotNull(message, "message");
  }
}
