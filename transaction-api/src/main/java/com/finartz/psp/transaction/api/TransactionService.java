package com.finartz.psp.transaction.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;
import static com.lightbend.lagom.javadsl.api.Service.topic;

import akka.Done;
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.broker.kafka.KafkaProperties;

/**
 * The Transaction service interface.
 */
public interface TransactionService extends Service {

  /**
   * Example: curl http://localhost:9000/api/transactions
   */
  ServiceCall<CreateTxnRequest, TxnCreatedResponse> create();

  /**
   * This gets published to Kafka.
   */
  Topic<TransactionEvent> transactionEvents();

  @Override
  default Descriptor descriptor() {
    // @formatter:off
    return named("transactions").withCalls(
            pathCall("/api/transactions/:id", this::create)
    ).withAutoAcl(true);
    // @formatter:on
  }
}
