package com.finartz.psp.transaction.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import lombok.Value;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = TransactionEvent.TransactionCreated.class, name = "transaction-created")
})
public interface TransactionEvent {

  TransactionId getTransactionId();

  @Value
  final class TransactionCreated implements TransactionEvent {

    public final TransactionId transactionId;

    @JsonCreator
    public TransactionCreated(TransactionId transactionId) {
      this.transactionId = Preconditions.checkNotNull(transactionId, "transactionId");
    }
  }
}
