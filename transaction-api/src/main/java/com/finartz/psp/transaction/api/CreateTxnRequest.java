package com.finartz.psp.transaction.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import lombok.Value;

@Value
@JsonDeserialize
public final class CreateTxnRequest {

  public final String message;

  @JsonCreator
  public CreateTxnRequest(String message) {
    this.message = Preconditions.checkNotNull(message, "message");
  }
}
