package com.finartz.psp.transaction.api;

import lombok.Value;

import java.util.UUID;

@Value
public class TransactionId {
  public UUID id;

  public TransactionId() {
    this.id = UUID.randomUUID();
  }

  public TransactionId(UUID id) {
    this.id = id;
  }
}
