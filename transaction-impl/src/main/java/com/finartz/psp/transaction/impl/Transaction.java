package com.finartz.psp.transaction.impl;

import com.finartz.psp.transaction.impl.domain.TxnAggregate;
import com.finartz.psp.transaction.impl.domain.TxnState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.CompressedJsonable;

import java.util.Optional;

/**
 * The state for the {@link Transaction} entity.
 */
@SuppressWarnings("serial")
@JsonDeserialize
@Value
@Builder
@AllArgsConstructor
public final class Transaction implements CompressedJsonable {

  public final Optional<TxnAggregate> txnAggregate;
  public final TxnState txnState;

  public static Transaction initial() {
    return Transaction.builder()
            .txnState(TxnState.NOT_CREATED)
            .build();
  }

  public static Transaction created(TxnAggregate txnAggregate) {
    return Transaction.builder()
            .txnState(TxnState.CREATED)
            .txnAggregate(Optional.of(txnAggregate))
            .build();
  }


}