package com.finartz.psp.transaction.impl.domain;

public enum TxnState {
  NOT_CREATED, CREATED, TIMEOUT, INITIATED, REFUNDED, SUCCEEDED, FAILED
}
