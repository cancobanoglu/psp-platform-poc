package com.finartz.psp.transaction.impl;

import com.finartz.psp.transaction.impl.domain.TransactionId;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

/**
 * This interface defines all the events that the Transaction entity supports.
 * <p>
 * By convention, the events should be inner classes of the interface, which
 * makes it simple to get a complete picture of what events an entity has.
 */
public interface TransactionEvent extends Jsonable, AggregateEvent<TransactionEvent> {

  /**
   * Tags are used for getting and publishing streams of events. Each event
   * will have this tag, and in this case, we are partitioning the tags into
   * 4 shards, which means we can have 4 concurrent processors/publishers of
   * events.
   */
  AggregateEventShards<TransactionEvent> TAG = AggregateEventTag.sharded(TransactionEvent.class, 4);

  /**
   * An event that represents a change in transaction creation.
   */
  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class TransactionCreated implements TransactionEvent {

    public final TransactionId transactionId;

    @JsonCreator
    public TransactionCreated(TransactionId transactionId) {
      this.transactionId = Preconditions.checkNotNull(transactionId, "transactionId");
    }
  }

  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class TransactionInitiated implements TransactionEvent {

    public final TransactionId transactionId;

    @JsonCreator
    public TransactionInitiated(TransactionId transactionId) {
      this.transactionId = Preconditions.checkNotNull(transactionId, "transactionId");
    }
  }


  @Override
  default AggregateEventTagger<TransactionEvent> aggregateTag() {
    return TAG;
  }

}
