package com.finartz.psp.transaction.impl;

import java.util.Optional;

import com.finartz.psp.transaction.impl.domain.TransactionId;
import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.CompressedJsonable;
import com.lightbend.lagom.serialization.Jsonable;

import akka.Done;

/**
 * This interface defines all the commands that the Transaction entity supports.
 * <p>
 * By convention, the commands should be inner classes of the interface, which
 * makes it simple to get a complete picture of what commands an entity
 * supports.
 */
public interface TransactionCommand extends Jsonable {

  /**
   * A command to say hello to someone using the current greeting message.
   * <p>
   * The reply type is String, and will contain the message to say to that
   * person.
   */
  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class CreateTransaction implements TransactionCommand, PersistentEntity.ReplyType<String> {

    public final String txnId;

    @JsonCreator
    public CreateTransaction(String txnId) {
      this.txnId = Preconditions.checkNotNull(txnId, "txnId");
    }
  }

  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class InitiateTransaction implements TransactionCommand, PersistentEntity.ReplyType<String> {

    public final TransactionId id;

    @JsonCreator
    public InitiateTransaction(TransactionId id) {
      this.id = Preconditions.checkNotNull(id, "id");
    }
  }

}
