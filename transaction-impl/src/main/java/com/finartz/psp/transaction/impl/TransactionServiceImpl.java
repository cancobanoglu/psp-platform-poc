package com.finartz.psp.transaction.impl;

import akka.japi.Pair;
import com.finartz.psp.transaction.api.CreateTxnRequest;
import com.finartz.psp.transaction.api.TransactionId;
import com.finartz.psp.transaction.api.TransactionService;
import com.finartz.psp.transaction.api.TxnCreatedResponse;
import com.finartz.psp.transaction.impl.TransactionCommand.CreateTransaction;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import javax.inject.Inject;
import java.util.UUID;

/**
 * Implementation of the TransactionService.
 */
public class TransactionServiceImpl implements TransactionService {

  private final PersistentEntityRegistry persistentEntityRegistry;

  @Inject
  public TransactionServiceImpl(PersistentEntityRegistry persistentEntityRegistry) {
    this.persistentEntityRegistry = persistentEntityRegistry;
    persistentEntityRegistry.register(TransactionEntity.class);
  }

  @Override
  public ServiceCall<CreateTxnRequest, TxnCreatedResponse> create() {
    return request -> {
      // Look up the hello world entity for the given ID.
      String txnId = UUID.randomUUID().toString();
      PersistentEntityRef<TransactionCommand> ref = persistentEntityRegistry.refFor(TransactionEntity.class, txnId);
      // Ask the entity the Hello command.
      return ref.ask(new CreateTransaction(txnId))
              .thenApply(s -> new TxnCreatedResponse("transaction id returned"));
    };
  }

  @Override
  public Topic<com.finartz.psp.transaction.api.TransactionEvent> transactionEvents() {
    // We want to publish all the shards of the hello event
    return TopicProducer.taggedStreamWithOffset(TransactionEvent.TAG.allTags(), (tag, offset) ->

            // Load the event stream for the passed in shard tag
            persistentEntityRegistry.eventStream(tag, offset).map(eventAndOffset -> {

              // Now we want to convert from the persisted event to the published event.
              // Although these two events are currently identical, in future they may
              // change and need to evolve separately, by separating them now we save
              // a lot of potential trouble in future.
              com.finartz.psp.transaction.api.TransactionEvent eventToPublish;

              if (eventAndOffset.first() instanceof TransactionEvent.TransactionCreated) {
                TransactionEvent.TransactionCreated transactionCreated = (TransactionEvent.TransactionCreated) eventAndOffset.first();
                eventToPublish = new com.finartz.psp.transaction.api.TransactionEvent.TransactionCreated(new TransactionId(transactionCreated.transactionId.id));
              } else {
                throw new IllegalArgumentException("Unknown event: " + eventAndOffset.first());
              }

              // We return a pair of the translated event, and its offset, so that
              // Lagom can track which offsets have been published.
              return Pair.create(eventToPublish, eventAndOffset.second());
            })
    );
  }
}
