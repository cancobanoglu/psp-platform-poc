package com.finartz.psp.transaction.impl;

import com.finartz.psp.transaction.impl.domain.TransactionId;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.UUID;

/**
 * This is an event sourced entity. It has a state, {@link Transaction}, which
 * stores what the greeting should be (eg, "Hello").
 * <p>
 * Commands get translated to events, and it's the events that get persisted by
 * the entity. Each event will have an event handler registered for it, and an
 * event handler simply applies an event to the current state. This will be done
 * when the event is first created, and it will also be done when the entity is
 * loaded from the database - each event will be replayed to recreate the state
 * of the entity.
 */
@Slf4j
public class TransactionEntity extends PersistentEntity<TransactionCommand, TransactionEvent, Transaction> {

  /**
   * An entity can define different behaviours for different states, but it will
   * always start with an initial behaviour. This entity only has one behaviour.
   */
  @Override
  public Behavior initialBehavior(Optional<Transaction> snapshotState) {


    /*
     * Behaviour is defined using a behaviour builder. The behaviour builder
     * starts with a state, if this entity supports snapshotting (an
     * optimisation that allows the state itself to be persisted to combine many
     * events into one), then the passed in snapshotState may have a value that
     * can be used.
     *
     * Otherwise, the default state is to use the Hello greeting.
     */

    if (!snapshotState.isPresent()) {
      /* if the state is new it is a brand new transaction
       * notCreated will be called to create a TransactionCreatedEvent */
      return notCreated(Transaction.initial());
    } else {
      Transaction state = snapshotState.get();
      switch (state.getTxnState()) {
        /* if the transactionEntity actor alive, it is ready to meet
         * transaction commands */
        case NOT_CREATED:
          return notCreated(state);
        /* if the transactionEntity actor alive, there is already a transaction
         * with an id generated before, it's waiting for some commands to be updated*/
        case INITIATED:
          return initiated(state);
        case TIMEOUT:
          return timeout(state);
        case CREATED:
          return created(state);
        case REFUNDED:
          return refunded(state);
        case SUCCEEDED:
          return succeeded(state);
        case FAILED:
          return failed(state);
        default:
          throw new RuntimeException();
      }
    }
  }

  private Behavior notCreated(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);

    builder.setCommandHandler(TransactionCommand.CreateTransaction.class, (cmd, ctx) -> {
      log.info("CreateTransaction command is handling. Cmd is : {} ", cmd);
      TransactionEvent.TransactionCreated transactionCreated = new TransactionEvent.TransactionCreated(new TransactionId(UUID.fromString(cmd.txnId)));
      return ctx.thenPersist(transactionCreated,
              (e) -> ctx.reply(transactionCreated));
    });

    builder.setEventHandlerChangingBehavior(TransactionEvent.TransactionCreated.class, event ->
            created(Transaction.created(state().txnAggregate.get())));

    /*
     * change current state to new one
     */
    builder.setReadOnlyCommandHandler(TransactionCommand.CreateTransaction.class,
            // Get the greeting from the current state, and prepend it to the name
            // that we're sending
            // a greeting to, and reply with that message.
            (cmd, ctx) -> ctx.reply(state().txnState + ", " + cmd.txnId + "!"));

    return builder.build();
  }

  private Behavior created(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

  private Behavior initiated(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

  private Behavior timeout(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

  private Behavior refunded(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

  private Behavior succeeded(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

  private Behavior failed(Transaction state) {
    BehaviorBuilder builder = newBehaviorBuilder(state);
    // omitted
    return builder.build();
  }

}
