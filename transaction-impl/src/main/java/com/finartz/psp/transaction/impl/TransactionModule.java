package com.finartz.psp.transaction.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import com.finartz.psp.transaction.api.TransactionService;

/**
 * The module that binds the TransactionService so that it can be served.
 */
public class TransactionModule extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindService(TransactionService.class, TransactionServiceImpl.class);
  }
}
