package com.finartz.psp.stream.impl;

import akka.Done;
import akka.stream.javadsl.Flow;
import com.finartz.psp.transaction.api.TransactionEvent;
import com.finartz.psp.transaction.api.TransactionService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

/**
 * This subscribes to the TransactionService event stream.
 */
public class StreamSubscriber {

  @Inject
  public StreamSubscriber(TransactionService transactionService, StreamRepository repository) {
    // Create a subscriber
    transactionService.helloEvents().subscribe()
      // And subscribe to it with at least once processing semantics.
      .atLeastOnce(
        // Create a flow that emits a Done for each message it processes
        Flow.<TransactionEvent>create().mapAsync(1, event -> {

          if (event instanceof TransactionEvent.GreetingMessageChanged) {
            TransactionEvent.GreetingMessageChanged messageChanged = (TransactionEvent.GreetingMessageChanged) event;
            // Update the message
            return repository.updateMessage(messageChanged.getName(), messageChanged.getMessage());

          } else {
            // Ignore all other events
            return CompletableFuture.completedFuture(Done.getInstance());
          }
        })
      );

  }
}
